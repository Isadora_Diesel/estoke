from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    code = models.CharField(max_length=12)
    banner = models.ImageField(null=True, upload_to='produtos')
    quantity = models.PositiveIntegerField()


    def __str__(self):
        return self.code + ' - ' + self.name
